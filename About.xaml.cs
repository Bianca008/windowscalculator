﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Calculator
{
    public partial class About : Window
    {
        public string m_author;
        public static int m_cpuNumber;
        public string m_cpu;
        public string m_os;
        public string m_link;

        public About()
        {
            InitializeComponent();
            m_author = "Author: Lixandru Bianca";
            m_cpuNumber = Environment.ProcessorCount;
            m_cpu = "CPUs: " + m_cpuNumber;
            m_os = "OS: " + Environment.OSVersion.Version;
            m_link = @"https://intranet.unitbv.ro/";
        }

        public void Open_Link(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            System.Diagnostics.Process.Start("https://intranet.unitbv.ro/");
        }

        public string Author
        {
            get
            {
                return this.m_author;
            }
            set
            {
            }
        }

        public string CPU
        {
            get
            {
                return this.m_cpu;
            }
            set
            {
            }
        }

        public string OS
        {
            get
            {
                return this.m_os;
            }
            set
            {
            }
        }

        public string Link
        {
            get
            {
                return this.m_link;
            }
            set
            {
            }
        }
    }
}