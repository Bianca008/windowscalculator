﻿using System.Text.RegularExpressions;

namespace Calculator
{
    public class OperatorsPrecedence
    {
        public OperatorsPrecedence()
        {

        }

        public double Solve(string equation)
        {
            Operation operation = new Operation();
            operation.Parse(equation);
            double result = operation.SolveEcuation();

            return result;
        }
    }

    public class Operation
    {
        public Operation()
        {
            m_result = 0.0;
            m_minusAndPlus = new Regex("[+-]");
            m_multiplyAndDivide = new Regex("[*/]");
        }

        public Operation m_leftNumber { get; set; }

        public string m_operator { get; set; }

        public Operation m_rightNumber { get; set; }

        private double m_result;

        private Regex m_minusAndPlus;

        private Regex m_multiplyAndDivide;

        public void Parse(string equation)
        {
            var operatorLocation = m_minusAndPlus.Match(equation);

            if (!operatorLocation.Success)
            {
                operatorLocation = m_multiplyAndDivide.Match(equation);
            }

            if (operatorLocation.Success)
            {
                m_operator = operatorLocation.Value;
                m_leftNumber = new Operation();
                m_leftNumber.Parse(equation.Substring(0, operatorLocation.Index));
                m_rightNumber = new Operation();
                m_rightNumber.Parse(equation.Substring(operatorLocation.Index + 1));
            }
            else
            {
                m_operator = "=";
                m_result = double.Parse(equation);
            }

        }

        public double SolveEcuation()
        {
            switch (m_operator)
            {
                case "=":
                    break;
                case "+":
                    m_result = m_leftNumber.SolveEcuation() + m_rightNumber.SolveEcuation();
                    break;
                case "-":
                    m_result = m_leftNumber.SolveEcuation() - m_rightNumber.SolveEcuation();
                    break;
                case "*":
                    m_result = m_leftNumber.SolveEcuation() * m_rightNumber.SolveEcuation();
                    break;
                case "/":
                    m_result = m_leftNumber.SolveEcuation() / m_rightNumber.SolveEcuation();
                    break;
            }
            return m_result;
        }
    }
}
