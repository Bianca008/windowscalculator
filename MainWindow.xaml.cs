﻿using System;
using System.IO;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;

namespace Calculator
{
    public partial class MainWindow : Window
    {
        enum BinaryOperations
        {
            None,
            Plus,
            Minus,
            Divide,
            Multiply
        };

        enum UnaryOperations
        {
            None,
            Inverse,
            Power,
            SquareRoot
        }

        private bool m_isDigitActivate;
        private bool m_cascadateOp;
        private string m_separator;
        private string m_number;
        private double m_firstNumber;
        private double m_secondNumber;
        private double m_memorizedValue;
        private BinaryOperations m_binaryOperationType;
        private System.Windows.Forms.NotifyIcon notifyIcon;

        private void CalculateBinaryOperation()
        {
            switch (m_binaryOperationType)
            {
                case BinaryOperations.Plus:
                    m_firstNumber += m_secondNumber;
                    m_secondNumber = 0.0;
                    m_binaryOperationType = BinaryOperations.None;
                    break;
                case BinaryOperations.Minus:
                    m_firstNumber -= m_secondNumber;
                    m_secondNumber = 0.0;
                    m_binaryOperationType = BinaryOperations.None;
                    break;
                case BinaryOperations.Divide:
                    if (m_secondNumber != 0.0)
                    {
                        m_firstNumber /= m_secondNumber;
                        m_secondNumber = 0.0;
                        m_binaryOperationType = BinaryOperations.None;
                    }
                    else
                    {
                        ShowResult("Can't divide a number by zero!");
                        m_firstNumber = 0.0;
                    }
                    break;
                case BinaryOperations.Multiply:
                    m_firstNumber *= m_secondNumber;
                    m_secondNumber = 0.0;
                    m_binaryOperationType = BinaryOperations.None;
                    break;
            }
        }

        private void CalculateNumber(UnaryOperations type, bool isFirstTheResult = true)
        {
            switch (type)
            {
                case UnaryOperations.Inverse:
                    if (isFirstTheResult == true)
                        m_firstNumber = 1 / m_firstNumber;
                    else
                        m_secondNumber = 1 / m_secondNumber;
                    break;
                case UnaryOperations.Power:
                    if (isFirstTheResult == true)
                        m_firstNumber *= m_firstNumber;
                    else
                        m_secondNumber *= m_secondNumber;
                    break;
                case UnaryOperations.SquareRoot:
                    if (isFirstTheResult == true)
                    {
                        if (m_firstNumber >= 0)
                        {
                            m_firstNumber = Math.Sqrt(m_firstNumber);
                            ShowResult(m_firstNumber.ToString());
                        }
                        else
                        {
                            ShowResult("Negative root!");
                            m_firstNumber = 0.0;
                            m_number = null;
                        }
                    }
                    else
                    {
                        if (m_secondNumber > 0)
                        {
                            m_secondNumber = Math.Sqrt(m_secondNumber);
                            ShowResult(m_secondNumber.ToString());
                        }
                        else
                            ShowResult("Negative root");
                    }
                    break;
            }
        }

        private void CalculateUnaryOperation(UnaryOperations type)
        {
            if (m_firstNumber == 0.0 && m_number != null)
            {
                m_firstNumber = double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);
                m_number = null;
                CalculateNumber(type, true);
            }
            else
            if (m_secondNumber == 0.0 && m_number != null)
            {
                m_secondNumber = double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);
                m_number = null;
                CalculateNumber(type, false);
            }
            else
            if (m_secondNumber != 0.0)
            {
                CalculateNumber(type, false);
            }
            else
            if (m_firstNumber != 0.0)
            {
                CalculateNumber(type, true);
            }
        }

        private void ShowResult(string number)
        {
            numberTextBox.Clear();
            if (m_isDigitActivate == true)
            {
                StringHandler handler = new StringHandler(number);
                numberTextBox.Text = handler.GenerateNumber(number, m_separator);
            }
            else
                numberTextBox.Text = number;
            numberTextBox.Foreground = System.Windows.Media.Brushes.White;
            numberTextBox.TextAlignment = TextAlignment.Right;
            numberTextBox.FontSize = 35;
        }

        private void ZeroPressed(object sender, RoutedEventArgs e)
        {
            if (m_number == null || m_number.CompareTo("0") != 0)
                m_number += "0";
            ShowResult(m_number);
        }

        private void OnePressed(object sender, RoutedEventArgs e)
        {
            m_number += "1";
            ShowResult(m_number);
        }

        private void TwoPressed(object sender, RoutedEventArgs e)
        {
            m_number += "2";
            ShowResult(m_number);
        }

        private void ThreePressed(object sender, RoutedEventArgs e)
        {
            m_number += "3";
            ShowResult(m_number);
        }

        private void FourPressed(object sender, RoutedEventArgs e)
        {
            m_number += "4";
            ShowResult(m_number);
        }

        private void FivePressed(object sender, RoutedEventArgs e)
        {
            m_number += "5";
            ShowResult(m_number);
        }

        private void SixPressed(object sender, RoutedEventArgs e)
        {
            m_number += "6";
            ShowResult(m_number);
        }

        private void SevenPressed(object sender, RoutedEventArgs e)
        {
            m_number += "7";
            ShowResult(m_number);
        }

        private void EightPressed(object sender, RoutedEventArgs e)
        {
            m_number += "8";
            ShowResult(m_number);
        }

        private void NinePressed(object sender, RoutedEventArgs e)
        {
            m_number += "9";
            ShowResult(m_number);
        }

        private void AddPoint(object sender, RoutedEventArgs e)
        {
            if (m_number.IndexOf(".") == -1)
            {
                m_number += ".";
                ShowResult(m_number);
            }
        }

        private void CalculateNumbersBinary()
        {
            if (m_firstNumber == 0.0)
            {
                m_firstNumber = double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);
                m_number = null;
                ShowResult(m_firstNumber.ToString());
            }
            else
            if (m_secondNumber == 0.0)
            {
                if (m_number != null)
                {
                    m_secondNumber = double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);
                    m_number = null;
                }
                CalculateBinaryOperation();
                ShowResult(m_firstNumber.ToString());
            }
            else
            {
                CalculateBinaryOperation();
                ShowResult(m_firstNumber.ToString());
            }
        }

        private void Divide(object sender, RoutedEventArgs e)
        {
            if (m_cascadateOp)
                Divide();
            else
                m_number += "/";
        }

        private void Multiply(object sender, RoutedEventArgs e)
        {
            if (m_cascadateOp)
                Multiply();
            else
                m_number += "*";
        }

        private void Minus(object sender, RoutedEventArgs e)
        {
            if (m_cascadateOp)
                Minus();
            else
                m_number += "-";
        }

        private void Plus(object sender, RoutedEventArgs e)
        {
            if (m_cascadateOp)
                Plus();
            else
                m_number += "+";
        }

        private void Equal(object sender, RoutedEventArgs e)
        {
            if (m_cascadateOp)
                Equal();
            else
            {
                OperatorsPrecedence op = new OperatorsPrecedence();
                double result = op.Solve(m_number);
                ShowResult(result.ToString());
                m_number = result.ToString();
            }
        }

        private void Inverse(object sender, RoutedEventArgs e)
        {
            if ((m_firstNumber == 0.0 && m_number == null) || (m_number[0] == '0' && m_number.Length == 1))
            {
                numberTextBox.Text = "Can't divide a number by zero!";
                numberTextBox.Foreground = System.Windows.Media.Brushes.White;
                numberTextBox.TextAlignment = TextAlignment.Right;
                numberTextBox.FontSize = 35;
            }
            else
            {
                CalculateUnaryOperation(UnaryOperations.Inverse);
                if (m_secondNumber != 0.0)
                    ShowResult(m_secondNumber.ToString());
                else
                    ShowResult(m_firstNumber.ToString());
            }
        }

        private void Power(object sender, RoutedEventArgs e)
        {
            if (m_firstNumber == 0.0 && m_number == null) ;
            else
            {
                CalculateUnaryOperation(UnaryOperations.Power);
                if (m_secondNumber != 0.0)
                    ShowResult(m_secondNumber.ToString());
                else
                    ShowResult(m_firstNumber.ToString());
            }
        }

        private void SquareRoot(object sender, RoutedEventArgs e)
        {
            if (m_firstNumber == 0.0 && m_number == null) ;
            else
            {
                CalculateUnaryOperation(UnaryOperations.SquareRoot);
            }
        }

        private void EraseAll(object sender, RoutedEventArgs e)
        {
            m_firstNumber = 0.0;
            m_secondNumber = 0.0;
            m_number = null;
            ShowResult(m_firstNumber.ToString());
        }

        private void EraseSecond(object sender, RoutedEventArgs e)
        {
            m_number = null;
            ShowResult("0");
        }

        private void DeleteLastDigit(object sender, RoutedEventArgs e)
        {
            DeleteLastDigit();
        }

        private void ReverseSign(object sender, RoutedEventArgs e)
        {
            if (m_number != null)
            {
                if (m_number[0] == '-')
                    m_number = m_number.Remove(0, 1);
                else
                    m_number = "-" + m_number;

                ShowResult(m_number);
            }
            else
            {
                m_firstNumber *= -1;
                ShowResult(m_firstNumber.ToString());
            }
        }

        private void CalculatePercentage(object sender, RoutedEventArgs e)
        {
            if (m_secondNumber != 0.0)
                CalculateBinaryOperation();
            m_secondNumber = double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture) / 100.0 * m_firstNumber;
            if (m_secondNumber < 0)
                m_binaryOperationType = BinaryOperations.Plus;
            CalculateBinaryOperation();
            m_number = null;
            ShowResult(m_firstNumber.ToString());
        }

        private void Mstore(object sender, RoutedEventArgs e)
        {
            m_memorizedValue = double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);
            m_number = null;
        }

        private void Mclear(object sender, RoutedEventArgs e)
        {
            m_memorizedValue = 0.0;
        }

        private void Mrecall(object sender, RoutedEventArgs e)
        {
            if (m_secondNumber == 0.0)
                m_secondNumber = m_memorizedValue;
            else
                if (m_firstNumber == 0.0)
                m_firstNumber = m_memorizedValue;

            ShowResult(m_memorizedValue.ToString());
        }

        private void Madd(object sender, RoutedEventArgs e)
        {
            if (m_number != null)
            {
                if (m_firstNumber == 0.0)
                {
                    m_firstNumber = double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);
                    m_memorizedValue += m_firstNumber;
                }
                else
                    m_memorizedValue += double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);

                m_number = null;
            }
            else
                m_memorizedValue += m_firstNumber;
        }

        private void Msubstract(object sender, RoutedEventArgs e)
        {
            if (m_number != null)
            {
                if (m_firstNumber == 0.0)
                {
                    m_firstNumber = double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);
                    m_memorizedValue -= m_firstNumber;
                }
                else
                    m_memorizedValue -= double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);

                m_number = null;
            }
            else
                m_memorizedValue -= m_firstNumber;
        }

        private void GetKeyboardInput(object sender, KeyEventArgs e)
        {
            e.Handled = true;

            if (e.Key == Key.NumPad0)
                m_number += "0";

            if (e.Key == Key.NumPad1)
                m_number += "1";

            if (e.Key == Key.NumPad2)
                m_number += "2";

            if (e.Key == Key.NumPad3)
                m_number += "3";

            if (e.Key == Key.NumPad4)
                m_number += "4";

            if (e.Key == Key.NumPad5)
                m_number += "5";

            if (e.Key == Key.NumPad6)
                m_number += "6";

            if (e.Key == Key.NumPad7)
                m_number += "7";

            if (e.Key == Key.NumPad8)
                m_number += "8";

            if (e.Key == Key.NumPad9)
                m_number += "9";

            if (e.Key == Key.Decimal)
                m_number += ".";

            if (e.Key == Key.Add)
                Plus();

            if (e.Key == Key.Subtract)
                Minus();

            if (e.Key == Key.Multiply)
                Multiply();

            if (e.Key == Key.Divide)
                Divide();

            if (e.Key == Key.Enter)
                Equal();

            if (e.Key == Key.Back)
                DeleteLastDigit();

            if (e.Key == Key.Escape)
            {
                m_number = null;
                m_firstNumber = 0.0;
                m_secondNumber = 0.0;
                ShowResult("0");
            }

            if (m_number != null)
                ShowResult(m_number);
        }

        private void Plus()
        {
            CalculateNumbersBinary();

            m_binaryOperationType = BinaryOperations.Plus;
        }

        private void Minus()
        {
            if (m_number == null)
            {
                m_number += "-";
                ShowResult(m_number);
            }
            else
            {
                CalculateNumbersBinary();

                m_binaryOperationType = BinaryOperations.Minus;
            }
        }

        private void Multiply()
        {
            CalculateNumbersBinary();

            m_binaryOperationType = BinaryOperations.Multiply;
        }

        private void Divide()
        {
            CalculateNumbersBinary();

            m_binaryOperationType = BinaryOperations.Divide;
        }

        private void Equal()
        {
            if (m_number != null)
            {
                m_secondNumber = double.Parse(m_number, System.Globalization.CultureInfo.InvariantCulture);
                m_number = null;
            }
            CalculateBinaryOperation();
            if (m_firstNumber != 0.0)
                ShowResult(m_firstNumber.ToString());
        }

        private void DeleteLastDigit()
        {
            if (m_number != null)
            {
                m_number = m_number.Remove(m_number.Length - 1);
                ShowResult(m_number);
            }
            else
            {
                string textNumber = m_firstNumber.ToString();
                textNumber = textNumber.Remove(textNumber.Length - 1);
                if (!string.IsNullOrEmpty(textNumber))
                    m_firstNumber = double.Parse(textNumber, System.Globalization.CultureInfo.InvariantCulture);
                ShowResult(textNumber);
            }
        }

        private void Copy(object sender, RoutedEventArgs e)
        {
            System.Windows.Clipboard.SetData(System.Windows.DataFormats.Text, numberTextBox);
        }

        private void Cut(object sender, RoutedEventArgs e)
        {
            System.Windows.Clipboard.SetData(System.Windows.DataFormats.Text, numberTextBox);
            numberTextBox.Text = string.Empty;
            if (m_number != null)
                m_number = null;
            else
                if (m_firstNumber != 0.0)
                m_firstNumber = 0.0;
            m_binaryOperationType = BinaryOperations.None;
            ShowResult("0");
        }

        private void Paste(object sender, RoutedEventArgs e)
        {
            m_number = System.Windows.Clipboard.GetText();
            bool isOk = false;
            if (m_number[0] == '-')
                isOk = m_number.Substring(1).All(char.IsDigit);
            else
                isOk = m_number.All(char.IsDigit);

            if (isOk == true)
                ShowResult(m_number);
            else
                ShowResult("Can't paste characeters!");
        }

        private void About(object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.Show();
        }

        protected override void OnStateChanged(EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                notifyIcon.BalloonTipTitle = "Minimize Sucessful";
                notifyIcon.BalloonTipText = "Minimized the app ";
                notifyIcon.ShowBalloonTip(400);
                notifyIcon.Visible = true;
            }
            else if (this.WindowState == WindowState.Normal)
            {
                notifyIcon.Visible = false;
                this.ShowInTaskbar = true;
            }

            base.OnStateChanged(e);
        }

        void IconMouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }

        private void DigitsOption(object sender, RoutedEventArgs e)
        {
            if (m_isDigitActivate == false)
            {
                m_isDigitActivate = true;
                CultureInfo ci = CultureInfo.CurrentCulture;

                if (ci.Equals("en-GB"))
                    m_separator = ".";
                else
                    m_separator = ",";
                File.WriteAllText("separatorOfDigits.txt", m_separator);
            }
            else
                m_isDigitActivate = false;
        }

        private void OperatorsPrecedence(object sender, RoutedEventArgs e)
        {
            m_cascadateOp = false;
        }

        private void CascadeOperations(object sender, RoutedEventArgs e)
        {
            m_cascadateOp = true;
            m_firstNumber = Convert.ToDouble(m_number, CultureInfo.InvariantCulture);
        }

        public MainWindow()
        {
            InitializeComponent();
            m_isDigitActivate = false;
            m_cascadateOp = true;
            m_number = null;
            m_firstNumber = 0.0;
            m_secondNumber = 0.0;
            m_memorizedValue = 0.0;
            m_binaryOperationType = BinaryOperations.None;
            m_separator = File.ReadAllText("separatorOfDigits.txt");

            notifyIcon = new System.Windows.Forms.NotifyIcon();
            notifyIcon.Icon = new System.Drawing.Icon(
                            @"calculator.ico");
            notifyIcon.MouseDoubleClick +=
                new System.Windows.Forms.MouseEventHandler
                    (IconMouseDoubleClick);
        }
    }
}