﻿using System;
using System.Text.RegularExpressions;

namespace Calculator
{
    class StringHandler
    {
        private string m_string;

        public StringHandler(string str)
        {
            m_string = str;
        }

        private string AddSeparatorInNumber(string number, string separator)
        {
            if (number.Length == 0)
                return null;

            string reversedResult = "";
            int contor = 0;

            for (int index = number.Length - 1; index > -1; --index)
            {
                ++contor;
                if (contor % 3 == 0)
                    reversedResult += number[index] + separator;
                else
                    reversedResult += number[index];
            }
            char[] charArray = reversedResult.ToCharArray();
            Array.Reverse(charArray);
            string result = new string(charArray);
            if (result[0] != separator[0])
                return result;
            else
                return result.Substring(1, result.Length - 1);
        }

        private int GetPosition(string number)
        {
            for (int index = 0; index < number.Length; ++index)
                if (number[index] == '.')
                    return index;
            return -1;
        }

        public string GenerateNumber(string number, string separator)
        {
            if (number.Length == 0)
                return null;
            if (number.Length < 3)
                return number;

            string copyNumber = "";
            if (number.Contains('.'.ToString()) == false && number[0] == '0' && number.Length > 1)
                copyNumber += number.Substring(1);
            else
                copyNumber += number;
            Regex reg = new Regex(@"^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$");
            if (reg.IsMatch(copyNumber))
            {
                if (copyNumber.Contains('.'.ToString()) == false)
                    return AddSeparatorInNumber(copyNumber, separator);
                else
                {
                    int position = GetPosition(copyNumber);
                    if (position != copyNumber.Length - 1)
                        return AddSeparatorInNumber(copyNumber.Substring(0, position), separator) + copyNumber.Substring(position);
                    else
                        return AddSeparatorInNumber(copyNumber.Substring(0, position), separator) + ".";
                }
            }
            return copyNumber;
        }
    }
}
